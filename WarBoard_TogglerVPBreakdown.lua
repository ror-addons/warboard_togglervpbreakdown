if not WarBoard_TogglerVPBreakdown then WarBoard_TogglerVPBreakdown = {} end
local WarBoard_TogglerVPBreakdown = WarBoard_TogglerVPBreakdown
local modName = "WarBoard_TogglerVPBreakdown"
local modLabel = "VPBdown"

function WarBoard_TogglerVPBreakdown.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "VPBIcon", 0, 0) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 78, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "WarBoard_TogglerVPBreakdown.ToggleWindow")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerVPBreakdown.ShowStatus")
	end
end

function WarBoard_TogglerVPBreakdown.ToggleWindow()
	if(VPBreakdown.AllowedToShowDetailWindow()) then
		if(VPBreakdown.Opt.Showing == 1 and WindowGetShowing("VPBreakdownWindow")) then
			VPBreakdown.HideWindow()	
		elseif(VPBreakdown.Opt.Showing == 0) then
			VPBreakdown.ShowWindow()
		end
	else
		VPBreakdown.TempHideWindow()
	end
end

function WarBoard_TogglerVPBreakdown.ShowStatus()
	LibWBToggler.DefaultTooltip(modName, modLabel)
end

